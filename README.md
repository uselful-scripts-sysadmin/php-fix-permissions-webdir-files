# DESCRIPTION:

perm.php is a PHP script to change and set properly permissions for the PHP web projects. Use this if you have problems with this directive.

# USAGE:
- Copy perm.php file to website directorio directory (ex: ~/public_html/) and execute the file from your terminal CLI or root domain from web browser, ex: http://www.mydomain.com/perm.php
- Delete the file after execute this to avoid security risks.
